package com.sid.illustrate

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.azeesoft.lib.colorpicker.ColorPickerDialog


class MainActivity : AppCompatActivity() {
	private lateinit var drawingView: DrawingBoardView

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		drawingView = findViewById<DrawingBoardView>(R.id.drawingView)
	}

	public fun onShowBrushDialog(view: View) {
		val brushDialog = Dialog(this)
		brushDialog.setContentView(R.layout.dialog_brush)
		brushDialog.setTitle("Select Brush Size: ")
		brushDialog.show()

		val smallBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnSmallBrush)
		smallBrushBtn.setOnClickListener {
			drawingView.setBrushSize(4.0F)
			brushDialog.dismiss()
		}

		val mediumBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnMediumBrush)
		mediumBrushBtn.setOnClickListener {
			drawingView.setBrushSize(8.0F)
			brushDialog.dismiss()
		}

		val largeBrushBtn = brushDialog.findViewById<ImageButton>(R.id.btnLargeBrush)
		largeBrushBtn.setOnClickListener {
			drawingView.setBrushSize(12.0F)
			brushDialog.dismiss()
		}
	}

	public fun onShowColorPicker(view: View) {
		val colorPickerDialog = ColorPickerDialog.createColorPickerDialog(this)
		colorPickerDialog.setOnColorPickedListener { color, hexVal ->
			drawingView.setColor(color)
			colorPickerDialog.hide()
		}
		colorPickerDialog.show()
	}

}